
#include <stdio.h>

#define MAX 100

int *p;              // point to a region of free memory
int *top;            // points to the top of the stack
int *bot;            // points to the bottom of the stack

void push(int);
int pop(void);

int main()
{
   int x, y;
   char str[80];

   p = (int *)malloc(MAX * sizeof(int));    // obtain stack memory
   if (!p)
   {
      printf("Allocation Failure!\n");
      return 0;
   }
   top = p;
   bot = p + MAX - 1;

   printf("Four-Function Calculator\n");
   printf("Enter 'q' to quit and '.' to print the top content of the stack.\n");

   do
   {
      printf(": ");
      gets(str);
      switch (*str)
      {
      case '+':
         x = pop();
         y = pop();
         printf("Result = %d\n", x + y);
         push(x + y);
         break;
      case '-':
         x = pop();
         y = pop();
         printf("Result = %d\n", x - y);
         push(x - y);
         break;
      case '*':
         x = pop();
         y = pop();
         printf("Result = %d\n", x * y);
         push(x * y);
         break;
      case '/':
         x = pop();
         y = pop();
         if (x == 0)
         {
            printf("Divide-by-zero error!\n");
            break;
         }
         printf("Result = %d\n", y / x);
         push(y / x);
         break;
      case '.':            // Show the content of the stack's top in this case.
         x = pop();
         push(x);
         printf("Current value at the top of the stack: %d\n", x);
         break;
      default:
         push(atoi(str));
      }
   } while (*str != 'q');

   return 0;
}

// put an element on the stack
void push(int i)
{
   if (p > bot)
   {
      printf("Stack Overflow..!!\n");
      return;
   }
   *p = i;
   p++;
}

// retrieve the top element from the stack
int pop(void)
{
   p--;
   if (p < top)
   {
      printf("Stack Underflow!\n");
      return 0;
   }
   return *p;
}
