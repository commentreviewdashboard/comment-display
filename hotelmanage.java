import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Room {
    private int number;
    private boolean isOccupied;

    public Room(int number) {
        this.number = number;
        this.isOccupied = false;
    }

    public int getNumber() {
        return number;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }
}

class Hotel {
    private List<Room> rooms;

    public Hotel(int numberOfRooms) {
        rooms = new ArrayList<>();
        for (int i = 1; i <= numberOfRooms; i++) {
            rooms.add(new Room(i));
        }
    }

    public boolean checkIn(int roomNumber) {
        Room room = findRoom(roomNumber);
        if (room != null && !room.isOccupied()) {
            room.setOccupied(true);
            System.out.println("Room " + roomNumber + " checked in successfully.");
            return true;
        }
        System.out.println("Room " + roomNumber + " is already occupied or does not exist.");
        return false;
    }

    public boolean checkOut(int roomNumber) {
        Room room = findRoom(roomNumber);
        if (room != null && room.isOccupied()) {
            room.setOccupied(false);
            System.out.println("Room " + roomNumber + " checked out successfully.");
            return true;
        }
        System.out.println("Room " + roomNumber + " is not occupied or does not exist.");
        return false;
    }

    private Room findRoom(int roomNumber) {
        for (Room room : rooms) {
            if (room.getNumber() == roomNumber) {
                return room;
            }
        }
        return null;
    }
}

public class HotelManagementSystem {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of rooms in the hotel: ");
        int numberOfRooms = scanner.nextInt();

        Hotel hotel = new Hotel(numberOfRooms);

        boolean exit = false;

        while (!exit) {
            System.out.println("\nHotel Management System");
            System.out.println("-----------------------");
            System.out.println("1. Check-In");
            System.out.println("2. Check-Out");
            System.out.println("3. Exit");
            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    System.out.print("Enter the room number to check-in: ");
                    int checkInRoomNumber = scanner.nextInt();
                    hotel.checkIn(checkInRoomNumber);
                    break;
                case 2:
                    System.out.print("Enter the room number to check-out: ");
                    int checkOutRoomNumber = scanner.nextInt();
                    hotel.checkOut(checkOutRoomNumber);
                    break;
                case 3:
                    exit = true;
                    System.out.println("Exiting the system. Goodbye!");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        }

        scanner.close();
    }
}
